# card-shop

> Nuxt.js shopping cart demo

Based on Vue.js with Nuxt.js framework.  
Vue-js-modal plugin used for modal windows.  
Lodash used for data sorting and other
operations, axios - to fetch JSON data from remote sources.

![Products list](https://gitlab.com/fedor_dev/nuxt.js-shop-cart-example/raw/master/screen1.png)

![Cart](https://gitlab.com/fedor_dev/nuxt.js-shop-cart-example/raw/master/screen2.png)

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start
```

